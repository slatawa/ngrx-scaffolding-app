import { Action } from "@ngrx/store";

export const DISPLAY_RANDOM_NAME = "[Todos] DISPLAY_RANDOM_NAME";
export const FETCH_DATA = "[Todo] FETCH_DATA";
export const FETCH_DATA_SUCCESS = "[Todo] FETCH_DATA_SUCCESS";

export class DisplayRandomName implements Action {
  readonly type = DISPLAY_RANDOM_NAME;
}

export class FetchData implements Action {
  readonly type = FETCH_DATA;
}

export class FetchDataSuccess implements Action {
  readonly type = FETCH_DATA_SUCCESS;
  constructor(public payload: Array<object>) {}
}
